### 微信支付V3版本生成平台证书
根据官方文档的介绍，平台证书可以通过接口方式获取，但是那个接口需要旧的平台证书，所以陷入一个死循环，这里就是为了解决死循环，整理的一个简单文档，从这里下载平台证书，不需要旧的平台证书。这个平台证书，实际上可以微信支付后台直接下载，需要的参数都是有的，只是他们没开发这个功能  
必要安装软件：Jdk1.8以上，需要执行jar文件，openssl（非必要项，查看证书序列号，也可以从微信后台查看）
1. [申请API证书](https://pay.weixin.qq.com/index.php/core/cert/api_cert#/)，生成后，下载得到如下文件
```
1603037915_20211104_cert.p12
apiclient_cert.pem
apiclient_key.pem
```
2. [设置APIv3密钥](https://pay.weixin.qq.com/index.php/core/cert/api_cert#/)，只要32位英文或数字就行，例如：5545A8E5142C99B95D6D5D5CBA00555

3. 查看公钥的serial号码或从第1步中查看  
```
openssl x509 -in apiclient_cert.pem -noout -serial
//生成结果：serial=5545A8E5142C99B95D6D5D5CBA1134480A0F88D1  
```

4. 生成平台证书，下载[dependencies.jar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/3af5267f-2b69-474d-88ef-9741ba766403.jar)或者从[git仓库](https://github.com/wechatpay-apiv3/CertificateDownloader)下载，必须填入apiv3秘钥，serial号码，企业号，私钥，序列号，生成目录（./ 当前目录）  
```
java -jar certificatedownloader-1.2.0-jar-with-dependencies.jar  -k 5545A8E5142C99B95D6D5D5CBA00555 -m 1603037915 -f apiclient_key.pem -s 5545A8E5142C99B95D6D5D5CBA1134480A0F88D1 -o ./
```
如果生成的结果有报错，证明其中有参数填写错误，  
如果无报错，在当前目录生成wechatpay_42xxxxxxxxx63B0283AAB8D.pem文件，  
其中42xxxxxxxxx63B0283AAB8D为平台证书的序列号  

### 最终得到如下结果：
1. p12文件：1603037915_20211104_cert.p12
2. 应用秘钥：apiclient_cert.pem
3. 应用私钥：apiclient_key.pem
4. V3 key： 5545A8E5142C99B95D6D5D5CBA00555
5. 应用序列号：5545A8E5142C99B95D6D5D5CBA1134480A0F88D1
6. 平台证书：wechatpay_42xxxxxxxxx63B0283AAB8D.pem
7. 平台证书序列号：42xxxxxxxxx63B0283AAB8D
8. 商户号：1603037915

### 天天商城后台配置（[插件地址](https://ext.dcloud.net.cn/plugin?id=6767)）
目前只有商家转账到零钱需要用到V3接口，前端的支付依然是V2版本，配置步骤如下：  
找到配置项：系统管理-系统配置-transfers.payment.wxpay_v3，保存类型为对象，选择自己的应用，特别注意，在微信支付后台开通功能的时候，一定要设置白名单，否则无权限  
```
{
  "name": "微信",
  "payName": "微信支付",
  "appid": "wx473333702b26bdb",
  "mchid": "1603037915",
  "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhQveOv7Hh4FBHRr/Q==\n-----END PRIVATE KEY-----",
  "privateKeySerial": "5545A8E5142C99B95D6D5D5CBA1134480A0F88D1",
  "platformCertificate": "-----BEGIN CERTIFICATE-----\nMIID3DCCAsSgAwI8xriVr1BimDI=\n-----END CERTIFICATE-----",
  "platformCertificateSerial": "42xxxxxxxxx63B0283AAB8D",
  "key": "5545A8E5142C99B95D6D5D5CBA00555"
}
```
### 秘钥文件内容转换为一行小技巧，高手可忽略
1. 打开谷歌浏览器，打开任意页面，再按F12
2. 找到Console选项卡，这里可以执行简单的js
3. `-----BEGIN PRIVATE KEY....不管换行不换行，都是字符串` 一定要用这个引号框住apiclient_key.pem内的内容，按一下回车，自动把换行转化为\n

有任何问题，请加QQ群：298724327