/**
 * 中国时差
 */
let timeZone = 8;

/**
 * 获取当天时间戳，写入数据和取出数据，不需要增加时差
 */
function getTodayTime(day = 0, check = false) {
	let time = new Date();
	time.setMinutes(0);
	time.setSeconds(0);
	time.setMilliseconds(0)
	if (check) {
		//由于时差问题，我们的0点，是utc的前一天16点
		time.setHours(time.getHours() + timeZone);
		time.setHours(timeZone * -1);
	} else {
		time.setHours(0);
	}
	if (day != 0) {
		time.setDate(time.getDate() + day);
	}
	console.log("getTodayTime", day, check, time.getTime())
	return time.getTime();
}

/**
 * 根据时间戳，获得日期，存在8小时时差
 */
function getDateByTime(timestamp) {
	let time;
	if (timestamp) {
		time = new Date(timestamp);
	} else {
		time = new Date();
	}
	time.setHours(timeZone);
	return time.getDate();
}
/**
 * 获取当前的小时数，存在8小时时差
 */
function getNowHours() {
	let time = new Date();
	time.setHours(time.getHours() + timeZone);
	return time.getHours();
}

/**
 * 格式化时间戳 Y-m-d H:i:s
 * @param {String} format Y-m-d H:i:s
 * @param {Number} timestamp 时间戳   
 * @return {String}
 */
function dateFormat(format, timeStamp) {
	let _date;
	if (!timeStamp) {
		_date = new Date();
	} else {
		if (isNaN(timeStamp)) {

		} else if ('' + timeStamp.length <= 10) {
			timeStamp = +timeStamp * 1000;
		} else {
			timeStamp = +timeStamp;
		}
		_date = new Date(timeStamp);
	}
	let Y = _date.getFullYear(),
		m = _date.getMonth() + 1,
		d = _date.getDate(),
		H = _date.getHours(),
		i = _date.getMinutes(),
		s = _date.getSeconds();
	//周
	let week = {
		"0": "日",
		"1": "一",
		"2": "二",
		"3": "三",
		"4": "四",
		"5": "五",
		"6": "六"
	};
	if (/(E+)/.test(format)) {
		format = format.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "星期" : "周") : "") +
			week[_date
				.getDay() + ""]);
	}
	m = m < 10 ? '0' + m : m;
	d = d < 10 ? '0' + d : d;
	H = H < 10 ? '0' + H : H;
	i = i < 10 ? '0' + i : i;
	s = s < 10 ? '0' + s : s;

	return format.replace(/[YmdHis]/g, key => {
		return {
			Y,
			m,
			d,
			H,
			i,
			s
		} [key];
	});
}

function getObjectValue(info, fields = []) {
	if (fields.length > 0) {
		return fields.reduce(function(pre, key) {
			pre[key] = info[key]
			return pre;
		}, {})
	}
	return info;
}
module.exports = {
	getTodayTime,
	getDateByTime,
	getNowHours,
	dateFormat,
	getObjectValue
}
