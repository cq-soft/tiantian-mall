const UniPayService = require('./unipay.js')
const uniPay = require('uni-pay');
/**
 * 余额支付处理
 */
module.exports = class BankService extends UniPayService {
	constructor(ctx) {
		super(ctx)
		this.type = "bank";
		this.payname = "银行卡";
	}
	async prepay(userInfo) {
		return {}
	}
	/**
	 * 创建支付订单
	 */
	async app(order, userInfo, next, preview) {
		return res;
	}

	/**
	 * 资金退款到用户账号
	 * @param {Object} order
	 */
	async refund(order) {}
	async transfers(data, platform, desc) {
		return {
			code: 0,
			message: "线下手动处理"
		}
	}
}
