const UniPayService = require('./unipay.js')
const uniPay = require('uni-pay');
/**
 * 货到付款
 */
module.exports = class DeliveryService extends UniPayService {
	constructor(ctx) {
		super(ctx)
		this.type = "delivery";
		this.payname = "货到付款";
	}
	async prepay(userInfo) {
		return {
			key: this.type,
			icon: "icon_pay_delivery",
			color: "#ff5500",
			name: this.payname,
			confirm: true
		}
	}
	/**
	 * 创建支付订单
	 */
	async app(order, userInfo, next) {
		console.log("货到付款");
		let platform = this.ctx.context.PLATFORM;
		let res = {
			transaction_id: Math.random().toString().substr(2),
			outTradeNo: order.outTradeNo,
			platform: platform,
			tradeStatus: "TRADE_SUCCESS",
			totalFee: 0,
			type: this.type,
			name: this.payname
		};
		let res2 = await next(res);
		console.log("货到付款完成，回调逻辑", res2);
		return res;
	}
}
