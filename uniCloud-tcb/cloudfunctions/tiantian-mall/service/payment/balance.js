const UniPayService = require('./unipay.js')
const uniPay = require('uni-pay');
/**
 * 余额支付处理
 */
module.exports = class BalanceService extends UniPayService {
	constructor(ctx) {
		super(ctx)
		this.type = "balance";
		this.payname = "余额";
	}
	async prepay(userInfo) {
		return {
			key: this.type,
			icon: "icon_pay_balance",
			color: "#ffaa00",
			name: this.payname,
			notice: `(当前余额：￥${userInfo.balance/100})`,
			confirm: true
		}
	}
	/**
	 * 创建支付订单
	 */
	async app(order, userInfo, next, preview) {
		//扣除用户余额，然后回调支付成功
		let res = await this.service.user.user.editBalance(userInfo._id, order.total_fee * -1, order.title, 99,
			order, preview);
		if (res.code < 0) {
			return res;
		}
		res.totalFee = order.total_fee;
		res.outTradeNo = order.outTradeNo;
		res.type = this.type;
		await next(res);
		return res;
	}

	/**
	 * 资金退款到用户账号
	 * @param {Object} order
	 */
	async refund(order) {
		let money = order.refundFee;
		let payInfo = order.payInfo;
		if (order.totalFee == order.refundFee) {
			//全额退款
		}
		return await this.service.user.user.editBalance(order.uid, order.refundFee, order.refundDesc, 99, {
			_id: order.outTradeNo,
			type: "refund",
			payInfo
		});
	}
}
