const {
	Service
} = require('uni-cloud-router')
/**
 * 短信父类,需要配置uni短信，申请短信模板
 */
module.exports = class BasicService extends Service {
	constructor(ctx) {
		super(ctx)
		//小程序配置
		this.configs = this.ctx.getConfigs.config("sms-message.configs");
		//子集重构this.type
		this.type = "basic"
	}

	async send(content) {
		let data = await this.getMessageContent(content);
		if (!data) {
			return false;
		}
		let {
			phones
		} = content;
		//获取短信模板id//为避免与签名混淆，短信内容不可包含“【】”符号
		this.templateId = this.ctx.getConfigs.config(`sms-message.${this.type}.template_id`);
		if (!phones || !this.templateId) {
			console.log(phones, this.templateId)
			console.log("电话或模板为空")
			return false;
		}
		//phones可能是1个号码，也可能是号码数组，最多支持50个号码
		let sendResult = await this._sendMessage(phones, this.templateId, data)
		if (sendResult) {
			console.log("发送短信完成")
			return this.afterSend(content, data, sendResult)
		}
	}
	/**
	 * 子类继承此方法，用于构造发送的data
	 * @param {Object} content
	 */
	async getMessageContent(content) {

	}
	async afterSend(content, data, sendResult) {}

	getDateTime(t) {
		//发单时间
		let date = new Date();
		if (t) {
			data = new Date(t);
		}
		//8小时时差
		date.setHours(date.getHours() + 8)
		return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
	}

	/**
	 * 调用发送微信模板消息
	 * @param {Object} openid
	 * @param {Object} template_id
	 * @param {Object} miniprogram
	 * @param {Object} pushData
	 */
	async _sendMessage(phones, templateId, data, times = 0) {
		let phone, phoneList;
		if (typeof phones == "string") {
			phone = phones;
		} else {
			phoneList = phones;
		}
		//https://uniapp.dcloud.io/uniCloud/send-sms?id=%e7%9f%ad%e4%bf%a1%e5%8f%91%e9%80%81
		let {
			errCode,
			errMsg
		} = await uniCloud.sendSms({
			...this.configs,
			phone,
			phoneList,
			templateId,
			data
		})
		if (!errCode) {
			return true;
		}
		/* 错误码	错误
		1001	参数校验未通过,errMsg内会给出详细信息
		4000	参数错误
		4001	apiKey 不存在 或 templateId 不正确
		4002	请检查smsKey、smsSecret是否有误
		4003	服务空间或IP地址不在白名单中
		5000	服务错误，请联系DCloud进行排查
		5001	服务器异常，请重试！ */
		uniCloud.logger.error("errMsg", errCode, errMsg)
		if (times < 5 && [5000, 5001].indexOf(errCode) != -1) {
			uniCloud.logger.error("发送短信错误" + errCode + errMsg + " 重试+" + (times + 1))
			return this._sendMessage(phones, templateId, data, times + 1)
		}
		return false;
	}
}
