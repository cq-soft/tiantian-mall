引入vue-content-loader扩展，vue2需要引入旧版本，新版本只支持vue3，更多查看[扩展原文](https://github.com/egoist/vue-content-loader)
```
npm i vvue-content-loader@^0.2
```

package.json中增加如下内容
```
"dependencies": {
	"vue-content-loader": "^0.2.3"
},
```


index.vue中引入扩展组件
```
import {
	ContentLoader
} from 'vue-content-loader'

//注册到components里面，页面才能调用
export default {	
	components: {
		ContentLoader,
	},
}
```

页面view组件中，增加展示代码，参考[svg语法](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/rect)
```
<view>
	<ContentLoader width="300" height="700">
		<rect x="10" y="40" rx="5" width="280" height="120" />
		<circle cx="40" cy="190" r="20" />
		<circle cx="100" cy="190" r="20" />
		<circle cx="160" cy="190" r="20" />
		<circle cx="210" cy="190" r="20" />
		<circle cx="260" cy="190" r="20" />
		<rect x="10" y="220" rx="3" ry="3" width="80" height="80" />
		<rect x="100" y="220" rx="3" ry="3" width="80" height="80" />
		<rect x="190" y="220" rx="3" ry="3" width="80" height="80" />
		<rect x="10" y="310" rx="40" ry="40" width="280" height="60" />
		<rect x="20" y="380" rx="3" ry="3" width="220" height="10" />
		<rect x="20" y="400" rx="3" ry="3" width="170" height="10" />
		<rect x="20" y="420" rx="3" ry="3" width="250" height="10" />
		<rect x="20" y="440" rx="3" ry="3" width="200" height="10" />
		<rect x="20" y="460" rx="3" ry="3" width="80" height="10" />
	</ContentLoader>
</view>
```





