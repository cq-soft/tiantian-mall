## 天天商城 
基于uniapp+unicloud+uniId开发的多级分销电商系统，突破传统b2c运营模式，引入uniAd广告功能，实现0元可购物、积分+现金购物模式，玩一玩就能得到自己心仪的商品。  
本系统支持一键发布到H5，Android，iOS，微信小程序等小程序系统。  
增加开源学习版本，从[gitee仓库地址](https://gitee.com/cq-soft/tiantian-mall.git)更新。

### 小程序体验
![宴席助理](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-adaad390-fa4a-4f33-b3f2-412eeffa13a0/c53304e4-4e4a-45f9-ba60-4121ac3d1970.jpg?x-oss-process=image/resize,m_fill,w_200,h_200)
![永福市场小程序](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/eb960efa-b306-47c9-a481-a8043d3fc2bc.jpg?x-oss-process=image/resize,m_fill,w_200,h_200)
![这有米小程序](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/f6ce02f9-de81-4c78-b5b1-e30e1ef6f3dc.png?x-oss-process=image/resize,m_fill,w_200,h_200)
![惠民](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/b782fca8-ebaa-4082-8d83-69b599e3f495.png?x-oss-process=image/resize,m_fill,w_200,h_200)

### 功能导读
![图](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/7e78ab50-b29f-4bd7-8045-1e0df4d22b90.png)

### 配套后台插件地址
[https://ext.dcloud.net.cn/plugin?id=6767](https://ext.dcloud.net.cn/plugin?id=6767)  

### 安装步骤
#### 在线安装（推荐）：
1. 在插件市场点击“导入插件并试用”或者购买源码授权版，
2. 同意付费插件试用协议，
3. 选择绑定的服务空间，
4. 勾选部署云函数，初始化db_init后（全部勾选），点击“直接部署服务空间”，
5. 如果有提示覆盖公共模块，点击“覆盖并重新部署”。  
6. 等待服务器自动执行安装操作

#### 手动安装：
1. 在插件市场点击“导入插件并试用”或者购买源码授权版，
2. 同意付费插件试用协议，
3. 选择绑定的服务空间，
4. 点击跳过
5. 继续导入HBuilderX（[安装最新编辑器](https://www.dcloud.io/hbuilderx.html)）
6. 编辑器提示新建项目
7. uniCloud目录右键，运行云服务空间安装向导
8. 选择第三步选择的云服务空间
9. 下一步，并开始部署

#### 微信小程序必须配置
request合法域名
```
https://api.bspapp.com;https://apis.map.qq.com;https://cos.ap-shanghai.myqcloud.com;https://tcb-api.tencentcloudapi.com;https://at.alicdn.com;https://api.next.bspapp.com  
```
uploadFile合法域名
```
https://api.bspapp.com;https://cos.ap-shanghai.myqcloud.com;https://file-uniinuyasf-mp-579d809f-3ae7-4bfe-ab04-b1c562fe9282.oss-cn-zhangjiakou.aliyuncs.com  
```
download合法域名
```
https://api.bspapp.com;https://cos.ap-shanghai.myqcloud.com;https://mp-579d809f-3ae7-4bfe-ab04-b1c562fe9282.cdn.bspapp.com  
```
如果依然报跨域问题，就把报错的域名一并加上

### uni-id插件相比官方新增功能
* 增加百度小程序登录，授权获取手机号
* 增加头条小程序登录
* 增加注册连带写入邀请者
* 增加推广渠道信息，uni-id-channel

### uni-pay插件相比官方新增功能
* 增加微信付款到零钱
* 增加服务商模式下单、添加特约商户分账接收方、完结分账

### 批量上传商品
#### 火车头批量采集并上传
1. 下载[采集模块](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/ff065592-9fac-4a2b-b201-51bd405314aa.wpm)，导入到火车头
2. web发布配置，选择“天天商城”，网页编码选择utf-8；全局变量填写/common/uni-config-center/tiantian-mall/config.json内apiToken值，此token不能为空；网址地址填写云函数URL化后，复制完整地址，例如：https://xxx.com/http/mall；登录方式选择不登录；上述配置完成后，点击获取列表，将获得商品分类；填写配置名后添加
3. 发布提交的字段包含：price，market_price，name，goods_banner_imgs（图片地址数组），category_id

### 系统参数配置
系统统一配置均依靠uni-config-center，原始目录是/uni_modules/uni-config-center/uniCloud/cloudfunctions/common/uni-config-center  编辑器自动映射后uniCloud/cloudfunctions/common/uni-config-center  
* uni-config-center/uni-id/config.json
	* 可修改app、小程序appid登录配置；
	* 短信key和secret；
	* 一键登录key和secret
* uni-config-center/tiantian-mall/config.json，
	* 可修改支付通知地址（domain）；
	* 支付类型（payment）与参数；
	* 广告位（adConfig）配置；
	* 微信企业转账到零钱配置（transfers）；
	* 短信登录模板id（service）；
	* 推送配置（push）；
	* 同城配送（delivery）；
	* 快递配送（express）；
	* 用户推荐团队（user）；
	* 前端自定义参数（customer）；
	* 前端分享（share）；
	* 商城分成返利、积分等（mall）
* uni-config-center/__UNI__9E9D6A0/config.json 如果一套后台，发布到多个应用，这个目录结构的参数，可以独立配置参数，与tiantian-mall/config.json合并，这里配置优先，可变参数例如：支付、广告
	
### 咨询QQ群
* 交流QQ群：298724327  

### 柔然科技承接软件定制开发
* 联系人：文经理
* QQ：343169893
* [柔然科技官网www.cqsort.com](http://www.cqsort.com/)

### 测试与案例
#### 测试地址 
H5预览 [http://tian.cqsort.com/](http://tian.cqsort.com/)  
后台体验
http://tian.cqsort.com/admin/  
账号：test  
密码：123456  

#### Android版客户案例
[惠民.apk](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3fbab731-e993-47e6-882f-a74e444709a3/7acd57cb-a07b-46e4-806b-09c96e736c00.apk)
